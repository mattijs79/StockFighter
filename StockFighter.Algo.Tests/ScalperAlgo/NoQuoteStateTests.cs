﻿using Moq;
using NUnit.Framework;
using Serilog;
using StockFighter.Algo.Scalper;
using StockFighter.Api;
using StockFighter.Api.Models;

namespace StockFighter.Algo.Tests.ScalperAlgo
{
    [TestFixture]
    public class NoQuoteStateTests
    {
        [Test]
        public void BadQuote()
        {
            var quote = new Quote { Ask = 10, Bid = 9, Last = 0 };

            var logger = Mock.Of<ILogger>();
            var state = new NoQuoteState(logger, GetParameters());

            var newState = state.NewQuote(quote);

            Assert.That(newState is NoQuoteState);
        }

        [Test]
        public void NewOrders()
        {
            var quote = new Quote { Ask = 1000, Bid = 900, Last = 950 };

            var logger = Mock.Of<ILogger>();
            var state = new NoQuoteState(logger, GetParameters());

            var newState = state.NewQuote(quote);

            Assert.That(newState is OrderOkState);

            Assert.That(((OrderOkState)newState).BuyOrder, Is.Not.Null);
            Assert.That(((OrderOkState)newState).BuyOrder.Direction, Is.EqualTo("buy"));
            Assert.That(((OrderOkState)newState).BuyOrder.Qty, Is.EqualTo(10000));
            Assert.That(((OrderOkState)newState).BuyOrder.Price, Is.EqualTo(890));

            Assert.That(((OrderOkState)newState).SellOrder, Is.Not.Null);
            Assert.That(((OrderOkState)newState).SellOrder.Direction, Is.EqualTo("sell"));
            Assert.That(((OrderOkState)newState).SellOrder.Qty, Is.EqualTo(100));
            Assert.That(((OrderOkState)newState).SellOrder.Price, Is.EqualTo(1100));
        }

        private static StartingParameters GetParameters()
        {
            var webApi = new Mock<IWebApi>();
            webApi
                .Setup(x => x.PlaceOrder(It.IsAny<OrderRequest>()))
                .Returns<OrderRequest>(x =>
                    new OrderResponse
                    {
                        Account = x.Account,
                        Direction = x.Direction,
                        Open = true,
                        OriginalQty = x.Qty,
                        RequestStatus = new RequestStatus { Ok = true, StatusCode = System.Net.HttpStatusCode.OK },
                        Symbol = x.Stock,
                        Venue = x.Venue,
                        Type = x.OrderType,
                        TotalFilled = 0,
                        Qty = x.Qty,
                        Price = x.Price
                    });

            webApi.Setup(x => x.CancelOrder(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new OrderResponse
                {
                    RequestStatus = new RequestStatus
                    {
                        Ok = true,
                        StatusCode = System.Net.HttpStatusCode.OK
                    }
                });

            return new StartingParameters(webApi.Object, 100, 1000, "accountName", "venueName", "symbol");
        }
    }
}
