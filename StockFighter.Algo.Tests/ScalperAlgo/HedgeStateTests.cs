﻿using Moq;
using NUnit.Framework;
using Serilog;
using StockFighter.Algo.Scalper;
using StockFighter.Api;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo.Tests.ScalperAlgo
{
    [TestFixture]
    public class HedgeStateTests
    {
        [Test]
        public void BidOrderNotFilled()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var bidOrder = new Order { Id = 1, Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();

            var state = new HedgeState(logger, GetParameters(), quote, bidOrder);
            var newState = state.OrderChanged(new Order { Id = 1, Qty = 10000, TotalFilled = 5000, Open = true });

            Assert.That(newState is HedgeState);
            Assert.That(object.ReferenceEquals(newState, state));
            Assert.That(((HedgeState)newState).BuyOrder, Is.Not.Null);
            Assert.That(((HedgeState)newState).BuyOrder.Open, Is.True);
            Assert.That(((HedgeState)newState).BuyOrder.Qty, Is.EqualTo(10000));
            Assert.That(((HedgeState)newState).BuyOrder.TotalFilled, Is.EqualTo(5000));
        }

        [Test]
        public void BidOrderFilled()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var bidOrder = new Order { Id = 1, Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();

            var state = new HedgeState(logger, GetParameters(), quote, bidOrder);
            var newState = state.OrderChanged(new Order { Id = 1, Qty = 10000, TotalFilled = 10000, Open = false });

            Assert.That(newState is QuoteOkState);
            Assert.That(((QuoteOkState)newState).BuyOrder, Is.Null);
            Assert.That(((QuoteOkState)newState).MostRecentQuote, Is.Not.True);
        }

        private static StartingParameters GetParameters()
        {
            var webApi = new Mock<IWebApi>();
            webApi
                .Setup(x => x.PlaceOrder(It.IsAny<OrderRequest>()))
                .Returns<OrderRequest>(x =>
                    new OrderResponse
                    {
                        Account = x.Account,
                        Direction = x.Direction,
                        Open = true,
                        OriginalQty = x.Qty,
                        RequestStatus = new RequestStatus { Ok = true, StatusCode = System.Net.HttpStatusCode.OK },
                        Symbol = x.Stock,
                        Venue = x.Venue,
                        Type = x.OrderType,
                        TotalFilled = 0,
                        Qty = x.Qty,
                        Price = x.Price
                    });

            webApi.Setup(x => x.CancelOrder(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new OrderResponse
                {
                    RequestStatus = new RequestStatus
                    {
                        Ok = true,
                        StatusCode = System.Net.HttpStatusCode.OK
                    }
                });

            return new StartingParameters(webApi.Object, 100, 1000, "accountName", "venueName", "symbol");
        }
    }
}
