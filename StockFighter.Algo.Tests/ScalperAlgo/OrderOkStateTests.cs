﻿using Moq;
using NUnit.Framework;
using Serilog;
using StockFighter.Algo.Scalper;
using StockFighter.Api;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo.Tests.ScalperAlgo
{
    [TestFixture]
    public class OrderOkStateTests
    {
        [Test]
        public void OldQuoteIsIgnored()
        {
            var quote = new Quote { QuoteTime = new DateTime(2016, 1, 1) };

            var logger = Mock.Of<ILogger>();
            var previousState = new QuoteOkState(logger, GetParameters(), quote);
            var state = new OrderOkState(previousState, new Order(), new Order());
            var newState = state.NewQuote(new Quote { QuoteTime = new DateTime(2015, 12, 31) });

            Assert.That(newState is OrderOkState);
            Assert.That(object.ReferenceEquals(state, newState));
        }

        [Test]
        public void BestBidMoved()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var askOrder = new Order { Price = 1100, Direction = "sell", Qty = 100 };
            var bidOrder = new Order { Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();
            var previousState = new QuoteOkState(logger, GetParameters(), quote);
            var state = new OrderOkState(previousState, bidOrder, askOrder);
            var newState = state.NewQuote(new Quote { Bid = 910, BidDepth = 20000, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 2) });

            Assert.That(newState is OrderOkState);
            Assert.That(object.ReferenceEquals(state, newState));

            // buy order moved
            Assert.That(((OrderOkState)newState).BuyOrder, Is.Not.Null);
            Assert.That(((OrderOkState)newState).BuyOrder.Direction, Is.EqualTo("buy"));
            Assert.That(((OrderOkState)newState).BuyOrder.Qty, Is.EqualTo(10000));
            Assert.That(((OrderOkState)newState).BuyOrder.Price, Is.EqualTo(900));

            // sell order remains the same
            Assert.That(((OrderOkState)newState).SellOrder, Is.Not.Null);
            Assert.That(((OrderOkState)newState).SellOrder.Direction, Is.EqualTo("sell"));
            Assert.That(((OrderOkState)newState).SellOrder.Qty, Is.EqualTo(100));
            Assert.That(((OrderOkState)newState).SellOrder.Price, Is.EqualTo(1100));
        }

        [Test]
        public void BidDepthTooSmall()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var askOrder = new Order { Price = 1100, Direction = "sell", Qty = 100 };
            var bidOrder = new Order { Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();
            var previousState = new QuoteOkState(logger, GetParameters(), quote);
            var state = new OrderOkState(previousState, bidOrder, askOrder);
            var newState = state.NewQuote(new Quote { Bid = 910, BidDepth = 2000, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 2) });

            Assert.That(newState is QuoteOkState);

            // buy order cancelled
            Assert.That(((QuoteOkState)newState).BuyOrder, Is.Null);

            // sell order cancelled
            Assert.That(((QuoteOkState)newState).SellOrder, Is.Null);
        }

        [Test]
        public void LastMoved()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var askOrder = new Order { Price = 1100, Direction = "sell", Qty = 100 };
            var bidOrder = new Order { Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();
            var previousState = new QuoteOkState(logger, GetParameters(), quote);
            var state = new OrderOkState(previousState, bidOrder, askOrder);
            var newState = state.NewQuote(new Quote { Bid = 910, BidDepth = 20000, Ask = 1000, Last = 951, QuoteTime = new DateTime(2016, 1, 2) });

            Assert.That(newState is QuoteOkState);

            // buy order cancelled
            Assert.That(((QuoteOkState)newState).BuyOrder, Is.Null);

            // sell order cancelled
            Assert.That(((QuoteOkState)newState).SellOrder, Is.Null);
        }

        [Test]
        public void SellOrderFilled()
        {
            var quote = new Quote { Bid = 900, Ask = 1000, Last = 950, QuoteTime = new DateTime(2016, 1, 1) };
            var askOrder = new Order { Id = 1, Price = 1100, Direction = "sell", Qty = 100 };
            var bidOrder = new Order { Id = 2, Price = 890, Direction = "buy", Qty = 10000 };

            var logger = Mock.Of<ILogger>();
            var previousState = new QuoteOkState(logger, GetParameters(), quote);
            var state = new OrderOkState(previousState, bidOrder, askOrder);
            var newState = state.OrderChanged(new Order { Id = 1, Open = false, TotalFilled = 100, Qty = 100, Price = 1100, Fills = new Fill[] { new Fill { Price = 1100, Qty = 100 } } } );

            Assert.That(newState is HedgeState);

            Assert.That(((HedgeState)newState).BuyOrder, Is.Not.Null);

            // buy order cancelled
            Assert.That(((HedgeState)newState).BuyOrder.Id, Is.Not.EqualTo(2));

            // there is a hedge order
            Assert.That(((HedgeState)newState).BuyOrder.Direction, Is.EqualTo("buy"));
            Assert.That(((HedgeState)newState).BuyOrder.Qty, Is.EqualTo(100));
            Assert.That(((HedgeState)newState).BuyOrder.Price, Is.EqualTo(900));
        }

        private static StartingParameters GetParameters()
        {
            var webApi = new Mock<IWebApi>();
            webApi
                .Setup(x => x.PlaceOrder(It.IsAny<OrderRequest>()))
                .Returns<OrderRequest>(x =>
                    new OrderResponse
                    {
                        Account = x.Account,
                        Direction = x.Direction,
                        Open = true,
                        OriginalQty = x.Qty,
                        RequestStatus = new RequestStatus { Ok = true, StatusCode = System.Net.HttpStatusCode.OK },
                        Symbol = x.Stock,
                        Venue = x.Venue,
                        Type = x.OrderType,
                        TotalFilled = 0,
                        Qty = x.Qty,
                        Price = x.Price
                    });

            webApi.Setup(x => x.CancelOrder(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new OrderResponse
                {
                    RequestStatus = new RequestStatus
                    {
                        Ok = true,
                        StatusCode = System.Net.HttpStatusCode.OK
                    }
                });

            return new StartingParameters(webApi.Object, 100, 1000, "accountName", "venueName", "symbol");
        }
    }
}
