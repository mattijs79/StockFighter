﻿using Serilog;
using StockFighter.Algo;
using StockFighter.Algo.PlaySellSide;
using StockFighter.Api;
using System;

namespace StockFighter
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .CreateLogger();

            var webapi = new WebApi(logger, "");

            var account = "FWB29812927";
            var venue = "TFITEX";
            var stock = "AFOO";

            var startingParameters = new StartingParameters(webapi, 500, 600, account, venue, stock);

            var buyAlgoState = new Algo.DodgeTheBulldozer.NoQuoteState(logger, true, startingParameters);
            var sellAlgoState = new Algo.DodgeTheBulldozer.NoQuoteState(logger, false, startingParameters);

            var algo = new MarketMakerAlgo(logger, startingParameters, buyAlgoState, sellAlgoState);

            var algoRunner = new AlgoRunner(logger, webapi, algo, new[] { new Subscription(account, venue, stock) } );
            algoRunner.Run();

            Console.ReadLine();

            algoRunner.Stop();

            Console.ReadLine();
        }
    }
}
