﻿using System.Net;

namespace StockFighter.Api
{
    public interface IRequestModelInjector
    {
        void InjectIntoRequest(HttpWebRequest request);
    }
}
