﻿using Newtonsoft.Json;
using StockFighter.Api.Models;
using System;
using System.IO;
using System.Net;

namespace StockFighter.Api
{
    internal static class WebApiCall<TResponse> where TResponse : IResponse, new()
    {
        internal static TResponse Get(string apiKey, string url)
        {
            return WebApiCall<TResponse>.Call("GET", apiKey, url, new EmptyRequestModelInjector());
        }

        internal static TResponse Delete(string apiKey, string url)
        {
            return WebApiCall<TResponse>.Call("DELETE", apiKey, url, new EmptyRequestModelInjector());
        }

        internal static TResponse Post(string apiKey, string url, IRequestModelInjector requestModelInjector)
        {
            return WebApiCall<TResponse>.Call("POST", apiKey, url, requestModelInjector);
        }

        internal static TResponse Call(string requestMethod, string apiKey, string url, IRequestModelInjector requestModelInjector)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Method = requestMethod;

                request.Headers.Add("X-Starfighter-Authorization", apiKey);

                requestModelInjector.InjectIntoRequest(request as HttpWebRequest);

                using (var response = (HttpWebResponse)request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();

                    var requestStatus = JsonConvert.DeserializeObject<RequestStatus>(json);
                    requestStatus.StatusCode = response.StatusCode;

                    TResponse model;
                    if (requestStatus.Ok)
                    {
                        model = JsonConvert.DeserializeObject<TResponse>(json);
                    }
                    else
                    {
                        model = new TResponse();
                    }
                    model.RequestStatus = requestStatus;

                    response.Close();

                    return model;
                }
            }
            catch (WebException e)
            {
                var response = e.Response as HttpWebResponse;

                if (response != null)
                {
                    return new TResponse { RequestStatus = new RequestStatus { Error = e.Message, Ok = false, StatusCode = response.StatusCode } };
                }
                else
                {
                    throw;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
