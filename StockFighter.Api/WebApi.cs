﻿using Newtonsoft.Json;
using Serilog;
using StockFighter.Api.Models;
using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StockFighter.Api
{
    public class WebApi : IWebApi
    {
        private class Subscription
        {
            public string Key { get; set; }

            public CancellationTokenSource TokenSource { get; set; }

            public Uri Uri { get; set; }

            public DateTime LastPublication { get; set; }
        }

        private readonly ResourceManager resourceManager = new ResourceManager(typeof(Urls));
        private readonly string apiKey;
        private ILogger logger;
        private readonly ConcurrentDictionary<string, Subscription> subscriptions = new ConcurrentDictionary<string, Subscription>();

        private const int WebSocketBufferSize = 1024;

        public event EventHandler<QuoteEmbeddedResponse> Quote = delegate { };

        public event EventHandler<Execution> Execution = delegate { };

        public event EventHandler<string> Unsubscription = delegate { };

        public WebApi(ILogger logger, string apiKey)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (string.IsNullOrWhiteSpace(apiKey))
            {
                throw new ArgumentNullException("apiKey");
            }

            this.logger = logger;
            this.apiKey = apiKey;
        }

        public Heartbeat GetHeartbeat()
        {
            return WebApiCall<Heartbeat>.Get(this.apiKey, this.resourceManager.GetString("Heartbeat"));
        }

        public VenueStatus GetVenueStatus(string venueName)
        {
            return WebApiCall<VenueStatus>.Get(this.apiKey, this.resourceManager.GetString("VenueStatus").Replace(":venue", venueName.ToUpper()));
        }

        public StockList GetStocks(string venueName)
        {
            return WebApiCall<StockList>.Get(this.apiKey, this.resourceManager.GetString("StockList").Replace(":venue", venueName.ToUpper()));
        }

        public Orderbook GetOrderbook(string venueName, string symbol)
        {
            return WebApiCall<Orderbook>.Get(
                this.apiKey,
                this.resourceManager
                .GetString("Orderbook")
                .Replace(":venue", venueName.ToUpper())
                .Replace(":stock", symbol.ToUpper()));
        }

        public OrderResponse PlaceOrder(OrderRequest request)
        {
            return WebApiCall<OrderResponse>.Post(
                this.apiKey,
                this.resourceManager.GetString("PlaceOrder")
                .Replace(":venue", request.Venue.ToUpper())
                .Replace(":stock", request.Stock.ToUpper()),
                new RequestModelInjector<OrderRequest>(request));
        }

        public QuoteResponse GetQuote(string venueName, string symbol)
        {
            return WebApiCall<QuoteResponse>.Get(
                this.apiKey,
                this.resourceManager
                .GetString("Quote")
                .Replace(":venue", venueName.ToUpper())
                .Replace(":stock", symbol.ToUpper()));
        }

        public OrderResponse GetOrderStatus(string venueName, string symbol, long id)
        {
            return WebApiCall<OrderResponse>.Get(
                this.apiKey,
                this.resourceManager
                .GetString("GetOrderStatus")
                .Replace(":venue", venueName.ToUpper())
                .Replace(":stock", symbol.ToUpper())
                .Replace(":id", id.ToString()));
        }

        public OrderResponse CancelOrder(string venueName, string symbol, long id)
        {
            return WebApiCall<OrderResponse>.Delete(
                this.apiKey,
                this.resourceManager
                .GetString("CancelOrder")
                .Replace(":venue", venueName.ToUpper())
                .Replace(":stock", symbol.ToUpper())
                .Replace(":order", id.ToString()));
        }

        public OrderList GetAllOrders(string venueName, string account, string symbol = "")
        {
            if (string.IsNullOrWhiteSpace(symbol))
            {
                return WebApiCall<OrderList>.Get(
                    this.apiKey,
                    this.resourceManager
                    .GetString("GetAllOrdersStock")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":account", account.ToUpper())
                    .Replace(":stock", symbol.ToUpper()));
            }
            else
            {
                return WebApiCall<OrderList>.Get(
                    this.apiKey,
                    this.resourceManager
                    .GetString("GetAllOrders")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":account", account.ToUpper()));
            }
        }

        public void SubscribeToQuotes(string venueName, string account, string symbol = "")
        {
            string url;
            if (string.IsNullOrWhiteSpace(symbol))
            {
                url = this.resourceManager
                    .GetString("SubscribeToQuotes")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":trading_account", account.ToUpper());
            }
            else
            {
                url = this.resourceManager
                    .GetString("SubscribeToQuotesStock")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":trading_account", account.ToUpper())
                    .Replace(":stock", symbol.ToUpper());
            }

            string key = this.GetQuoteSubscriptionKey(venueName, account, symbol);

            this.Subscribe(key, new Uri(url), this.HandleSerializedQuote);
        }

        public void SubscribeToExecutions(string venueName, string account, string symbol = "")
        {
            string url;
            if (string.IsNullOrWhiteSpace(symbol))
            {
                url = this.resourceManager
                    .GetString("SubscribeToExecutions")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":trading_account", account.ToUpper());
            }
            else
            {
                url = this.resourceManager
                    .GetString("SubscribeToExecutionsStock")
                    .Replace(":venue", venueName.ToUpper())
                    .Replace(":trading_account", account.ToUpper())
                    .Replace(":stock", symbol.ToUpper());
            }

            string key = this.GetQuoteSubscriptionKey(venueName, account, symbol);

            this.Subscribe(key, new Uri(url), this.HandleSerializedExecution);
        }

        public void UnSubscribeFromQuotes(string venueName, string account, string symbol = "")
        {
            this.UnSubscribe(this.GetQuoteSubscriptionKey(venueName, account, symbol));
        }

        public void UnSubscribeFromExecutions(string venueName, string account, string symbol = "")
        {
            this.UnSubscribe(this.GetExecutionSubscriptionKey(venueName, account, symbol));
        }

        private void HandleSerializedExecution(string json)
        {
            try
            {
                var requestStatus = JsonConvert.DeserializeObject<RequestStatus>(json);

                var execution = JsonConvert.DeserializeObject<Execution>(json);
                execution.RequestStatus = requestStatus;

                this.Execution(this, execution);
            }
            catch (Exception e)
            {
                this.logger.Error(e, "WebApi.HandleSerializedExecution Error");
            }
        }

        private void HandleSerializedQuote(string json)
        {
            try
            {
                var requestStatus = JsonConvert.DeserializeObject<RequestStatus>(json);

                var quote = JsonConvert.DeserializeObject<QuoteEmbeddedResponse>(json);
                quote.RequestStatus = requestStatus;

                this.Quote(this, quote);
            }
            catch (Exception e)
            {
                this.logger.Error(e, "WebApi.HandleSerializedQuote Error");
            }
        }

        private string GetQuoteSubscriptionKey(string venueName, string account, string symbol = "")
        {
            return string.Format("q{0}{1}{2}", venueName, account, symbol);
        }

        private string GetExecutionSubscriptionKey(string venueName, string account, string symbol = "")
        {
            return string.Format("e{0}{1}{2}", venueName, account, symbol);
        }

        private void Subscribe(string key, Uri uri, Action<string> handleMessageAction)
        {
            if (!this.subscriptions.ContainsKey(key))
            {
                var task = Task.Run(async () =>
                {
                    await RunWebSocket(uri, key, handleMessageAction);
                });
            }
        }

        private void UnSubscribe(string key)
        {
            Subscription subscription;

            if (this.subscriptions.TryRemove(key, out subscription))
            {
                subscription.TokenSource.Cancel();

                this.Unsubscription(this, key);
            }
        }

        private Task RunWebSocket(Uri uri, string subscriptionKey, Action<string> handleMessageAction)
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            var subscription = new Subscription { Key = subscriptionKey, TokenSource = tokenSource, Uri = uri };
            this.subscriptions.AddOrUpdate(subscriptionKey, subscription, (key, value) => subscription);

            return this.RunWebSocketAsync(subscription, handleMessageAction);
        }

        private async Task RunWebSocketAsync(Subscription subscription, Action<string> handleMessageAction)
        {
            try
            {
                var ws = new ClientWebSocket();

                await ws.ConnectAsync(subscription.Uri, subscription.TokenSource.Token);

                if (ws.State == WebSocketState.Open)
                {
                    byte[] buffer = new byte[WebSocketBufferSize];

                    var builder = new StringBuilder();

                    while (!subscription.TokenSource.Token.IsCancellationRequested)
                    {
                        var result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), subscription.TokenSource.Token);

                        builder.Append(Encoding.UTF8.GetString(buffer, 0, result.Count));

                        if (result.MessageType == WebSocketMessageType.Text && result.EndOfMessage)
                        {
                            handleMessageAction(builder.ToString());

                            builder.Clear();

                            subscription.LastPublication = DateTime.UtcNow;
                        }
                    }
                }
            }
            catch (WebSocketException e)
            {
                this.logger.Error(e, "WebApi.RunWebSocket Error");

                // Reconnect
                await this.RunWebSocketAsync(subscription, handleMessageAction);
            }
            catch (Exception e)
            {
                this.logger.Error(e, "WebApi.RunWebSocket Error");

                // Reconnect
                await this.RunWebSocketAsync(subscription, handleMessageAction);
            }
        }
    }
}
