﻿using StockFighter.Api.Models;
using System;

namespace StockFighter.Api
{
    public interface IWebApi
    {
        event EventHandler<QuoteEmbeddedResponse> Quote;

        event EventHandler<Execution> Execution;

        event EventHandler<string> Unsubscription;

        Heartbeat GetHeartbeat();

        VenueStatus GetVenueStatus(string venueName);

        StockList GetStocks(string venueName);

        Orderbook GetOrderbook(string venueName, string symbol);

        OrderResponse PlaceOrder(OrderRequest request);

        QuoteResponse GetQuote(string venueName, string symbol);

        OrderResponse GetOrderStatus(string venueName, string symbol, long id);

        OrderResponse CancelOrder(string venueName, string symbol, long id);

        OrderList GetAllOrders(string venueName, string account, string symbol = "");

        void SubscribeToQuotes(string venueName, string account, string symbol = "");

        void SubscribeToExecutions(string venueName, string account, string symbol = "");

        void UnSubscribeFromQuotes(string venueName, string account, string symbol = "");

        void UnSubscribeFromExecutions(string venueName, string account, string symbol = "");        
    }
}
