﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Text;

namespace StockFighter.Api
{
    public class RequestModelInjector<TRequest> : IRequestModelInjector
    {
        private readonly TRequest requestModel;

        public RequestModelInjector(TRequest requestModel)
        {
            if (requestModel == null)
            {
                this.requestModel = requestModel;
            }

            this.requestModel = requestModel;
        }

        public void InjectIntoRequest(HttpWebRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            byte[] buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(requestModel));
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = buffer.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
            }
        }
    }
}
