﻿namespace StockFighter.Api.Models
{
    public class QuoteResponse : Quote, IResponse
    {
        public RequestStatus RequestStatus { get; set; }
    }
}
