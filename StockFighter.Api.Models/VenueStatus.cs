﻿namespace StockFighter.Api.Models
{
    public class VenueStatus : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public string Venue { get; set; }
    }
}
