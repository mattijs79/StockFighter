﻿using System;

namespace StockFighter.Api.Models
{
    public class Fill
    {
        public int Price { get; set; }

        public int Qty { get; set; }

        public DateTime Ts { get; set; }
    }
}
