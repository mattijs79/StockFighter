﻿namespace StockFighter.Api.Models
{
    public class OrderRequest
    {
        public string Account { get; set; }

        public string Venue { get; set; }

        public string Stock { get; set; }

        public int Price { get; set; }

        public int Qty { get; set; }

        public string Direction { get; set; }

        public string OrderType { get; set; }
    }
}