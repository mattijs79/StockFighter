﻿using System;

namespace StockFighter.Api.Models
{
    public class Quote
    {
        public string Symbol { get; set; }

        public string Venue { get; set; }

        public int Bid { get; set; }

        public int Ask { get; set; }

        public int BidSize { get; set; }

        public int AskSize { get; set; }

        public long BidDepth { get; set; }

        public long AskDepth { get; set; }

        public int Last { get; set; }

        public int LastSize { get; set; }

        public DateTime LastTrade { get; set; }

        public DateTime QuoteTime { get; set; }
    }
}
