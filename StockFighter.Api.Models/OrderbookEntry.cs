﻿namespace StockFighter.Api.Models
{
    public class OrderbookEntry
    {
        public int Price { get; set; }

        public int Qty { get; set; }

        public bool IsBuy { get; set; }
    }
}
