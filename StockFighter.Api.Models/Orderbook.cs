﻿using System;
using System.Collections.Generic;

namespace StockFighter.Api.Models
{
    public class Orderbook : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public string Venue { get; set; }

        public string Symbol { get; set; }

        public DateTime Ts { get; set; }

        public IReadOnlyList<OrderbookEntry> Bids { get; set; }

        public IReadOnlyList<OrderbookEntry> Asks { get; set; }
    }
}
