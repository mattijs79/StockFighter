﻿using System;
using System.Collections.Generic;

namespace StockFighter.Api.Models
{
    public class Order
    {
        public Order()
        {
            this.Fills = new List<Fill>();
        }
        public string Symbol { get; set; }

        public string Venue { get; set; }

        public string Direction { get; set; }

        public int OriginalQty { get; set; }

        public int Qty { get; set; }

        public int Price { get; set; }

        public string Type { get; set; }

        public long Id { get; set; }

        public string Account { get; set; }

        public DateTime Ts { get; set; }

        public IReadOnlyList<Fill> Fills { get; set; }

        public int TotalFilled { get; set; }

        public bool Open { get; set; }
    }
}
