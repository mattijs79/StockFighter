﻿using System;

namespace StockFighter.Api.Models
{
    public class Execution : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public bool Ok { get; set; }

        public string Symbol { get; set; }

        public Order Order { get; set; }

        public int StandingId { get; set; }

        public int IncomingId { get; set; }

        public int Price { get; set; }

        public DateTime FilledAt { get; set; }

        public bool StandingComplete { get; set; }

        public bool IncomingComplete { get; set; }
    }
}
