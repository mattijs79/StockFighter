﻿namespace StockFighter.Api.Models
{
    public class OrderResponse : Order, IResponse
    {
        public RequestStatus RequestStatus { get; set; }
    }
}
