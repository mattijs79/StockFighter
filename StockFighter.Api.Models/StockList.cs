﻿using System.Collections.Generic;

namespace StockFighter.Api.Models
{
    public class StockList : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public IReadOnlyList<Stock> Symbols { get; set; }
    }
}
