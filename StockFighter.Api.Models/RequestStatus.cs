﻿using System.Net;

namespace StockFighter.Api.Models
{
    public class RequestStatus
    {
        public HttpStatusCode StatusCode { get; set; }

        public bool Ok { get; set; }

        public string Error { get; set; }
    }
}
