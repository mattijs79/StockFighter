﻿using System.Collections.Generic;

namespace StockFighter.Api.Models
{
    public class OrderList : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public IReadOnlyList<Order> Orders { get; set; }
    }
}
