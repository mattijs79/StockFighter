﻿namespace StockFighter.Api.Models
{
    public interface IResponse
    {
        RequestStatus RequestStatus { get; set;  }
    }
}
