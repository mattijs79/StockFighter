﻿namespace StockFighter.Api.Models
{
    public class Heartbeat : IResponse
    {
        public RequestStatus RequestStatus { get; set; }
    }
}
