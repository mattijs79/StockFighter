﻿namespace StockFighter.Api.Models
{
    public class QuoteEmbeddedResponse : IResponse
    {
        public RequestStatus RequestStatus { get; set; }

        public Quote Quote { get; set; }
    }
}
