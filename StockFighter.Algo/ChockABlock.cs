﻿using StockFighter.Api;
using System;
using System.Linq;
using System.Threading;

namespace StockFighter.Algo
{
    public class ChockABlock
    {
        private readonly IWebApi webApi;

        public ChockABlock(IWebApi webApi)
        {
            if (webApi == null)
            {
                throw new ArgumentNullException("webApi");
            }

            this.webApi = webApi;
        }

        public void Run(int shareCount, string accountName, string venueName, string symbol)
        {
            decimal openVolume = shareCount;

            while (openVolume > 0)
            {
                var orderbook = this.webApi.GetOrderbook(venueName, symbol);

                if (orderbook.Asks != null && orderbook.Asks.Count > 0)
                {
                    var price = orderbook.Asks.OrderBy(x => x.Price).FirstOrDefault();

                    if (price != null)
                    {
                        var response = this.webApi.PlaceOrder(new Api.Models.OrderRequest
                        {
                            Account = accountName,
                            Direction = "buy",
                            Price = price.Price,
                            Qty = (int)Math.Min(price.Qty, openVolume),
                            Stock = symbol,
                            Venue = venueName,
                            OrderType = "market"
                        });

                        if (!response.RequestStatus.Ok)
                        {
                            Console.WriteLine("Error: {0}", response.RequestStatus.Error);

                            break;
                        }

                        Console.WriteLine("Placed order {0} of {1} @ {2}", response.Id, response.OriginalQty, response.Price);

                        if (response.Open)
                        {
                            for (int i = 0; i < 10; i++)
                            {
                                var order = this.webApi.GetOrderStatus(venueName, symbol, response.Id);

                                if (!order.Open)
                                {
                                    openVolume -= order.TotalFilled;

                                    Console.WriteLine("Order {0} still closed", order.Id);

                                    break;
                                }

                                Console.WriteLine("Order {0} still open", order.Id);

                                Thread.Sleep(1);
                            }
                        }
                        else
                        {
                            openVolume -= response.TotalFilled;
                        }
                    }
                    Console.WriteLine("Still to be traded: {0}", openVolume);

                    Thread.Sleep(5);
                }
            }
        }
    }
}
