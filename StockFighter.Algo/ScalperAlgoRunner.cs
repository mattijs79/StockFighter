﻿using Serilog;
using StockFighter.Algo.Interfaces;
using StockFighter.Algo.Scalper.Interfaces;
using StockFighter.Api;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo
{ 
    public class ScalperAlgoRunner : IAlgorithm
    {
        private readonly ILogger logger;
        private readonly StartingParameters startingParameters;
        private IScalperOrderAlgo algoState;
        private Quote mostRecentQuote;

        private readonly RiskManagement riskManagement = new RiskManagement();

        public ScalperAlgoRunner(ILogger logger, StartingParameters startingParameters, IScalperOrderAlgo algoState)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (startingParameters == null)
            {
                throw new ArgumentNullException("startingParameters");
            }

            if (algoState == null)
            {
                throw new ArgumentNullException("algoState");
            }

            this.logger = logger;
            this.startingParameters = startingParameters;
            this.algoState = algoState;
        }

        public decimal? GetProjectedProfit()
        {
            if (this.mostRecentQuote != null)
            {
                return this.riskManagement.GetProjectedProfit(this.mostRecentQuote);
            }

            return null;
        }

        public void NewQuote(Quote quote)
        {
            this.mostRecentQuote = quote;

            try
            {
                this.algoState = this.algoState.NewQuote(quote);
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "ScalperAlgoRunner.NewQuote Exception on state {0}", this.algoState.StateName);

                throw;
            }

            this.PrintState();
        }

        public void OrderChanged(Order order)
        {
            try
            {
                this.riskManagement.OrderChanged(order);

                this.algoState = this.algoState.OrderChanged(order);

                this.PrintState();
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "ScalperAlgoRunner.OrderChanged");

                throw;
            }
        }

        private void PrintState()
        {
            if (this.algoState.MostRecentQuote != null)
            {
                Console.Clear();

                Console.WriteLine("Bid {0} {1} {2} Ask", this.algoState.MostRecentQuote.Bid, this.algoState.MostRecentQuote.Last, this.algoState.MostRecentQuote.Ask);

                int longPosition = this.riskManagement.LongPosition;
                int shortPosition = this.riskManagement.ShortPosition;

                Console.WriteLine("Current state buy: {0} Long: {1} Short: {2}",
                    this.algoState.StateName,
                    longPosition, shortPosition);

                Console.WriteLine("Net position: {0}",
                    longPosition - shortPosition);

                Console.WriteLine("Projected profit: {0}", this.GetProjectedProfit());
            }
        }
    }
}
