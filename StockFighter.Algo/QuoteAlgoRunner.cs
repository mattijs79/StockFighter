﻿using Serilog;
using StockFighter.Algo.Interfaces;
using StockFighter.Api;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo
{
    public class QuoteAlgoRunner : IAlgorithm
    {
        private readonly ILogger logger;
        private readonly StartingParameters startingParameters;
        private IQuoteOrderAlgo buyAlgoState;
        private IQuoteOrderAlgo sellAlgoState;
        private Quote mostRecentQuote;
        
        private readonly RiskManagement riskManagement = new RiskManagement();

        public QuoteAlgoRunner(ILogger logger, StartingParameters startingParameters, IQuoteOrderAlgo buyAlgoState, IQuoteOrderAlgo sellAlgoState)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (startingParameters == null)
            {
                throw new ArgumentNullException("startingParameters");
            }

            if (buyAlgoState == null)
            {
                throw new ArgumentNullException("buyAlgoState");
            }

            if (sellAlgoState == null)
            {
                throw new ArgumentNullException("sellAlgoState");
            }

            this.logger = logger;
            this.startingParameters = startingParameters;
            this.buyAlgoState = buyAlgoState;
            this.sellAlgoState = sellAlgoState;
        }

        public decimal? GetProjectedProfit()
        {
            if (this.mostRecentQuote != null)
            {
                return this.riskManagement.GetProjectedProfit(this.mostRecentQuote);
            }

            return null;
        }

        public void NewQuote(Quote quote)
        {
            this.mostRecentQuote = quote;

            int netPosition = this.riskManagement.LongPosition - this.riskManagement.ShortPosition;

            try
            {
                this.buyAlgoState = this.buyAlgoState.NewQuote(quote, netPosition);
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "QuoteAlgoRunner.NewQuote Exception on buy state {0}", this.buyAlgoState.StateName);

                throw;
            }

            try
            {
                this.sellAlgoState = this.sellAlgoState.NewQuote(quote, netPosition);
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "QuoteAlgoRunner.NewQuote Exception on sell state {0}", this.sellAlgoState.StateName);

                throw;
            }

            this.PrintState();
        }

        public void OrderChanged(Order order)
        {
            try
            {
                this.riskManagement.OrderChanged(order);

                int netPosition = this.riskManagement.LongPosition - this.riskManagement.ShortPosition;

                this.buyAlgoState = this.buyAlgoState.OrderChanged(order, netPosition);

                this.sellAlgoState = this.sellAlgoState.OrderChanged(order, netPosition);

                this.PrintState();
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "QuoteAlgoRunner.OrderChanged");

                throw;
            }
        }

        private void PrintState()
        {
            if (this.buyAlgoState.MostRecentQuote != null && this.sellAlgoState.MostRecentQuote != null)
            {
                Console.Clear();

                Console.WriteLine("Bid {0} {1} Ask", this.buyAlgoState.MostRecentQuote.Bid, this.sellAlgoState.MostRecentQuote.Ask);

                int longPosition = this.riskManagement.LongPosition;
                int shortPosition = this.riskManagement.ShortPosition;

                Console.WriteLine("Current state buy: {0} Long position: {1}",
                    this.buyAlgoState.StateName,
                    longPosition);

                Console.WriteLine("Current state sell: {0} Short position: {1}",
                    this.sellAlgoState.StateName,
                    shortPosition);

                Console.WriteLine("Net position: {0}",
                    longPosition - shortPosition);

                Console.WriteLine("Projected profit: {0}", this.GetProjectedProfit());
            }
        }
    }
}
