﻿using StockFighter.Api;
using System;

namespace StockFighter.Algo
{
    public class StartingParameters
    {
        public readonly IWebApi WebApi;
        public readonly int OrderSize;
        public readonly int MaxNetPosition;
        public readonly string AccountName;
        public readonly string VenueName;
        public readonly string Symbol;

        public StartingParameters(IWebApi webApi, int orderSize, int maxNetPosition, string accountName, string venueName, string symbol)
        {
            if (webApi == null)
            {
                throw new ArgumentNullException("webApi");
            }

            if (string.IsNullOrWhiteSpace(accountName))
            {
                throw new ArgumentNullException("accountName");
            }

            if (string.IsNullOrWhiteSpace(venueName))
            {
                throw new ArgumentNullException("venueName");
            }

            if (string.IsNullOrWhiteSpace(symbol))
            {
                throw new ArgumentNullException("symbol");
            }

            this.WebApi = webApi;
            this.OrderSize = orderSize;
            this.MaxNetPosition = maxNetPosition;
            this.AccountName = accountName;
            this.VenueName = venueName;
            this.Symbol = symbol;
        }
    }
}
