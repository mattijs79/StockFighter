﻿using Serilog;
using StockFighter.Algo.Interfaces;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo.QuoteMachine
{
    public abstract class AlgoStateBase : IQuoteOrderAlgo
    {
        public readonly StartingParameters StartingParameters;
        public readonly ILogger Logger;

        public Order CurrentOrder { get; protected set; }
        public Quote MostRecentQuote { get; protected set; }

        public virtual string StateName { get { return "AlgoStateBase"; } }

        public bool BuySide { get; protected set; }

        public AlgoStateBase(ILogger logger, bool buySide, StartingParameters startingParameters, Quote bestQuote, Order currentOrder)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (startingParameters == null)
            {
                throw new ArgumentNullException("startingParameters");
            }

            this.Logger = logger;
            this.BuySide = buySide;
            this.StartingParameters = startingParameters;
            this.MostRecentQuote = bestQuote;
            this.CurrentOrder = currentOrder;
        }

        public virtual IQuoteOrderAlgo NewQuote(Quote quote, int netPosition)
        {
            return this;
        }

        public virtual IQuoteOrderAlgo OrderChanged(Order order, int netPosition)
        {
            return this;
        }

        protected int GetOrderPrice(Quote quote)
        {
            int lowVolumePremium = this.BuySide ? (int)(1 + (Math.Max(0, this.StartingParameters.OrderSize - quote.BidDepth) / 10m))
                : (int)(1 + (Math.Max(0, this.StartingParameters.OrderSize - quote.AskDepth) / 10m));

            return (int)(this.BuySide ? quote.Bid - (quote.Last * 0.05 * lowVolumePremium) : quote.Last + (quote.Last * 0.05 * lowVolumePremium));
        }
    }
}
