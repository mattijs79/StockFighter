﻿using Serilog;
using StockFighter.Algo.Interfaces;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo.QuoteMachine
{
    public class NoQuoteState : AlgoStateBase, IQuoteOrderAlgo
    {
        public override string StateName { get { return "NoQuoteState"; } }

        public NoQuoteState(ILogger logger, bool buySide, StartingParameters parameters)
            : base(logger, buySide, parameters, null, null)
        {
        }

        public override IQuoteOrderAlgo NewQuote(Quote quote, int netPosition)
        {
            int newNetPositionIfFilled = base.BuySide ? netPosition + this.StartingParameters.OrderSize : netPosition - this.StartingParameters.OrderSize;

            if (Math.Abs(newNetPositionIfFilled) > this.StartingParameters.MaxNetPosition)
            {
                this.Logger.Information("NoQuoteState: not placing {0} order since it would lead to a net position of {1}", base.BuySide ? "buy" : "sell", newNetPositionIfFilled);

                return this;
            }

            var response = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
            {
                Account = this.StartingParameters.AccountName,
                Direction = base.BuySide ? "buy" : "sell",
                Price = base.GetOrderPrice(quote),
                Qty = this.StartingParameters.OrderSize,
                Stock = this.StartingParameters.Symbol,
                Venue = this.StartingParameters.VenueName,
                OrderType = "limit"
            });

            this.Logger.Information("NoQuoteState. Added {0} limit order of {1} @ {2}", base.BuySide ? "buy" : "sell", response.Qty, response.Price);

            if (!response.RequestStatus.Ok)
            {
                this.Logger.Error("NoQuoteState. Error: {0}", response.RequestStatus.Error);

                return new NoOrderState(this, quote);
            }

            this.Logger.Information("NoQuoteState. Switching to OrderOkState");

            return new OrderOkState(this, quote, response);
        }
    }
}
