﻿using StockFighter.Algo.Interfaces;
using StockFighter.Api.Models;
using System;

namespace StockFighter.Algo.QuoteMachine
{
    public class NoOrderState : AlgoStateBase, IQuoteOrderAlgo
    {
        public override string StateName { get { return "NoOrderState"; } }

        public NoOrderState(AlgoStateBase previousState)
            : base(previousState.Logger, previousState.BuySide, previousState.StartingParameters, previousState.MostRecentQuote, null)
        {
        }

        public NoOrderState(AlgoStateBase previousState, Quote quoteBestAsk)
            : base(previousState.Logger, previousState.BuySide, previousState.StartingParameters, quoteBestAsk, null)
        {
        }

        public override IQuoteOrderAlgo NewQuote(Quote quote, int netPosition)
        {
            if (quote.QuoteTime <= base.MostRecentQuote.QuoteTime)
            {
                return this;
            }

            if (quote.Last > 0)
            {
                this.MostRecentQuote = quote;

                int newNetPositionIfFilled = base.BuySide ? netPosition + this.StartingParameters.OrderSize : netPosition - this.StartingParameters.OrderSize;

                if (Math.Abs(newNetPositionIfFilled) > this.StartingParameters.MaxNetPosition)
                {
                    this.Logger.Information("NoOrderState: not placing {0} order since it would lead to a net position of {1}", base.BuySide ? "buy" : "sell", newNetPositionIfFilled);

                    return this;
                }

                var response = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
                {
                    Account = this.StartingParameters.AccountName,
                    Direction = base.BuySide ? "buy" : "sell",
                    Price = base.GetOrderPrice(quote),
                    Qty = this.StartingParameters.OrderSize,
                    Stock = this.StartingParameters.Symbol,
                    Venue = this.StartingParameters.VenueName,
                    OrderType = "limit"
                });

                this.Logger.Information("Added sell limit order of {0} @ {1}", response.Qty, response.Price);

                if (!response.RequestStatus.Ok)
                {
                    this.Logger.Error("Error: {0}", response.RequestStatus.Error);

                    return this;
                }

                this.Logger.Information("Switching to OrderOkState");

                return new OrderOkState(this, response);
            }

            return this;
        }
    }
}
