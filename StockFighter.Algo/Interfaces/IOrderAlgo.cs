﻿using StockFighter.Api.Models;

namespace StockFighter.Algo.Interfaces
{
    public interface IQuoteOrderAlgo
    {
        string StateName { get; }

        Order CurrentOrder { get; }

        Quote MostRecentQuote { get; }

        IQuoteOrderAlgo NewQuote(Quote quote, int netPosition);

        IQuoteOrderAlgo OrderChanged(Order order, int netPosition);
    }
}
