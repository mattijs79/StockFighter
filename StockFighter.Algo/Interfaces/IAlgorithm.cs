﻿using StockFighter.Api.Models;

namespace StockFighter.Algo.Interfaces
{
    public interface IAlgorithm
    {
        void OrderChanged(Order order);

        void NewQuote(Quote quote);
    }
}
