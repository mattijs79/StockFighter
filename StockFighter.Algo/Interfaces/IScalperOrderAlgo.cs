﻿using StockFighter.Api.Models;

namespace StockFighter.Algo.Scalper.Interfaces
{
    public interface IScalperOrderAlgo
    {
        string StateName { get; }

        Quote MostRecentQuote { get; }

        IScalperOrderAlgo NewQuote(Quote quote);

        IScalperOrderAlgo OrderChanged(Order order);
    }
}
