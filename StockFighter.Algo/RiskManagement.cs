﻿using StockFighter.Api.Models;
using System.Collections.Generic;
using System.Linq;

namespace StockFighter.Api
{
    public class RiskManagement
    {
        private Dictionary<long, Order> buyOrders = new Dictionary<long, Order>();
        private Dictionary<long, Order> sellOrders = new Dictionary<long, Order>();

        public int LongPosition
        {
            get
            {
                return this.buyOrders.Sum(x => x.Value.Fills.Sum(y => y.Qty));
            }
        }

        public int ShortPosition
        {
            get
            {
                return this.sellOrders.Sum(x => x.Value.Fills.Sum(y => y.Qty));
            }
        }

        public int BuyVolumeOpen
        {
            get
            {
                return this.buyOrders.Where(x => x.Value.Open).Sum(x => x.Value.Qty - x.Value.TotalFilled);
            }
        }

        public decimal AmountBought
        {
            get { return this.buyOrders.Sum(x => x.Value.Fills.Sum(y => y.Qty * y.Price / 100m)); }
        }

        public decimal AmountSold
        {
            get { return this.sellOrders.Sum(x => x.Value.Fills.Sum(y => y.Qty * y.Price / 100m)); }
        }

        public int SellVolumeOpen
        {
            get
            {
                return this.sellOrders.Where(x => x.Value.Open).Sum(x => x.Value.Qty - x.Value.TotalFilled);
            }
        }

        public decimal? GetProjectedProfit(Quote quote)
        {
            decimal profit = this.AmountSold - this.AmountBought;

            int netPosition = this.LongPosition - this.ShortPosition;

            profit += netPosition * quote.Last / 100m;

            return profit;
        }

        public void OrderChanged(Order order)
        {
            if (order.Direction == "buy")
            {
                if (order.Open)
                {
                    if (this.buyOrders.ContainsKey(order.Id)) this.buyOrders[order.Id] = order;
                    else this.buyOrders.Add(order.Id, order);
                }
                else if (order.Fills.Count == 0 && this.buyOrders.ContainsKey(order.Id))
                {
                    // No need to keep unfilled cancelled orders
                    this.buyOrders.Remove(order.Id);
                }
            }

            if (order.Direction == "sell")
            {
                if (order.Open)
                {
                    if (this.sellOrders.ContainsKey(order.Id)) this.sellOrders[order.Id] = order;
                    else this.sellOrders.Add(order.Id, order);
                }
                else if (order.Fills.Count == 0 && this.sellOrders.ContainsKey(order.Id))
                {
                    // No need to keep unfilled cancelled orders
                    this.sellOrders.Remove(order.Id);
                }
            }
        }
    }
}
