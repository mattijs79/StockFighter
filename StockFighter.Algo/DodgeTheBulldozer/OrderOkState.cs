﻿using StockFighter.Algo.Interfaces;
using StockFighter.Api.Models;

namespace StockFighter.Algo.DodgeTheBulldozer
{
    public class OrderOkState : AlgoStateBase, IQuoteOrderAlgo
    {
        public override string StateName { get { return "OrderOkState"; } }

        public OrderOkState(AlgoStateBase previousState, Order currentOrder)
            : base(previousState.Logger, previousState.BuySide, previousState.StartingParameters, previousState.MostRecentQuote, currentOrder)
        {
        }

        public OrderOkState(AlgoStateBase previousState, Quote quote, Order currentOrder)
            : base(previousState.Logger, previousState.BuySide, previousState.StartingParameters, quote, currentOrder)
        {
        }

        public override IQuoteOrderAlgo NewQuote(Quote quote, int netPosition)
        {
            if (quote.QuoteTime <= base.MostRecentQuote.QuoteTime)
            {
                return this;
            }

            if ((quote.Last > 0 && this.MostRecentQuote.Last != quote.Last) || base.Bulldozer(quote))
            {
                this.Logger.Information("OrderOkState. Cancelling {0} limit order {1} of {2} @ {3}", base.BuySide ? "buy" : "sell", base.CurrentOrder.Id, base.CurrentOrder.Qty, base.CurrentOrder.Price);

                var response = base.StartingParameters.WebApi.CancelOrder(base.StartingParameters.VenueName, base.StartingParameters.Symbol, base.CurrentOrder.Id);

                if (!response.RequestStatus.Ok)
                {
                    this.Logger.Error("OrderOkState. Error: {0}", response.RequestStatus.Error);

                    return this;
                }
                else
                {
                    this.Logger.Information("OrderOkState. Switching to NoOrder");

                    return new NoOrderState(this, quote);
                }
            }

            this.MostRecentQuote = quote;

            return this;
        }

        public override IQuoteOrderAlgo OrderChanged(Order order, int netPosition)
        {
            if (this.CurrentOrder.Id == order.Id)
            {
                if (order.Open)
                {
                    this.CurrentOrder = order;

                    return this;
                }
                else
                {
                    this.Logger.Information("OrderOkState. Switching to NoOrder");

                    return new NoOrderState(this);
                }
            }

            return this;
        }
    }
}
