﻿using System;

namespace StockFighter.Algo
{
    public class Subscription
    {
        public readonly string Account;

        public readonly string Venue;

        public readonly string Stock;

        public Subscription(string account, string venue, string stock)
        {
            if (string.IsNullOrWhiteSpace(account))
            {
                throw new ArgumentNullException("account");
            }

            if (string.IsNullOrWhiteSpace(venue))
            {
                throw new ArgumentNullException("venue");
            }

            if (string.IsNullOrWhiteSpace(stock))
            {
                throw new ArgumentNullException("stock");
            }

            this.Account = account;

            this.Venue = venue;

            this.Stock = stock;
        }
    }
}
