﻿using Serilog;
using StockFighter.Algo.Scalper.Interfaces;
using StockFighter.Api.Models;
using System;
using System.Linq;

namespace StockFighter.Algo.Scalper
{
    public class AlgoStateBase : IScalperOrderAlgo
    {
        protected const int Spread = 100;

        protected const int BidSize = 10000;
        protected const int BidDepthThreshold = 1500;

        public readonly StartingParameters StartingParameters;
        public readonly ILogger Logger;

        public Order SellOrder { get; protected set; }
        public Order BuyOrder { get; protected set; }
        public Quote MostRecentQuote { get; protected set; }

        public virtual string StateName { get { return "AlgoStateBase"; } }

        public AlgoStateBase(ILogger logger, StartingParameters startingParameters, Quote bestQuote, Order buyOrder, Order sellOrder)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (startingParameters == null)
            {
                throw new ArgumentNullException("startingParameters");
            }

            this.Logger = logger;
            this.StartingParameters = startingParameters;
            this.MostRecentQuote = bestQuote;
            this.BuyOrder = buyOrder;
            this.SellOrder = sellOrder;
        }

        public virtual IScalperOrderAlgo NewQuote(Quote quote)
        {
            return this;
        }

        public virtual IScalperOrderAlgo OrderChanged(Order order)
        {
            return this;
        }

        protected int GetBidPrice()
        {
            var orderBook = this.StartingParameters.WebApi.GetOrderbook(this.StartingParameters.VenueName, this.StartingParameters.Symbol);

            var bids = orderBook
                .Bids
                .GroupBy(x => x.Price)
                .OrderByDescending(g => g.Key)
                .Select(g => new OrderbookEntry { IsBuy = true, Price = g.Key, Qty = g.Sum(y => y.Qty) });

            if (!orderBook.RequestStatus.Ok)
            {
                throw new Exception(string.Format("AlgoStateBase.GetBidPrice: {0}", orderBook.RequestStatus.Error));
            }

            int cumQty = 0;
            foreach (var bidEntry in bids)
            {
                cumQty += bidEntry.Qty;

                if (cumQty >= BidDepthThreshold)
                {
                    return bidEntry.Price;
                }
            }

            return 0;
        }

        protected int GetAskPrice(Quote quote)
        {
            return quote.Ask + Spread;
        }

        protected void CancelOrder(Order order)
        {
            this.Logger.Debug("Cancelling {0} limit order {1} of {2} @ {3}", order.Direction, order.Id, order.Qty, order.Price);

            var response = this.StartingParameters.WebApi.CancelOrder(order.Venue, order.Symbol, order.Id);

            if (!response.RequestStatus.Ok)
            {
                this.Logger.Error("Error: {0}", response.RequestStatus.Error);
            }
        }

        protected bool BestBidMovedUp(Quote quote)
        {
            return quote.Bid > this.MostRecentQuote.Bid;
        }

        protected bool BestBidMovedDown(Quote quote)
        {
            return quote.Bid < this.MostRecentQuote.Bid;
        }

        protected bool BidSizeTooSmall(Quote quote)
        {
            return quote.BidDepth < BidDepthThreshold;
        }

        protected bool LastMoved(Quote quote)
        {
            return quote.Last != this.MostRecentQuote.Last;
        }
    }
}
