﻿using Serilog;
using StockFighter.Algo.Scalper.Interfaces;
using StockFighter.Api.Models;

namespace StockFighter.Algo.Scalper
{
    public class NoQuoteState : AlgoStateBase
    {
        public override string StateName { get { return "NoQuoteState"; } }

        public NoQuoteState(ILogger logger, StartingParameters parameters)
            : base(logger, parameters, null, null, null)
        {
            this.Logger.Information("Switching to NoQuoteState");
        }

        public override IScalperOrderAlgo NewQuote(Quote quote)
        {
            if (quote.Last > 0 && quote.Ask > 0 && quote.Bid > 0)
            {
                this.MostRecentQuote = quote;

                var bidPrice = base.GetBidPrice();

                if (bidPrice > 0)
                {
                    var sellOrderResponse = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
                    {
                        Account = this.StartingParameters.AccountName,
                        Direction = "sell",
                        Price = this.GetAskPrice(quote),
                        Qty = this.StartingParameters.OrderSize,
                        Stock = this.StartingParameters.Symbol,
                        Venue = this.StartingParameters.VenueName,
                        OrderType = "limit"
                    });

                    this.Logger.Debug("NoQuoteState: Added sell limit order of {0} @ {1}", sellOrderResponse.Qty, sellOrderResponse.Price);

                    if (!sellOrderResponse.RequestStatus.Ok)
                    {
                        this.Logger.Error("Error: {0}", sellOrderResponse.RequestStatus.Error);

                        return this;
                    }

                    var buyOrderResponse = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
                    {
                        Account = this.StartingParameters.AccountName,
                        Direction = "buy",
                        Price = bidPrice,
                        Qty = BidSize,
                        Stock = this.StartingParameters.Symbol,
                        Venue = this.StartingParameters.VenueName,
                        OrderType = "limit"
                    });

                    this.Logger.Debug("NoQuoteState: Added buy limit order of {0} @ {1}", buyOrderResponse.Qty, buyOrderResponse.Price);

                    if (!buyOrderResponse.RequestStatus.Ok)
                    {
                        base.CancelOrder(sellOrderResponse);

                        return this;
                    }


                    return new OrderOkState(this, buyOrderResponse, sellOrderResponse);
                }
            }

            return this;
        }
    }
}
