﻿using Serilog;
using StockFighter.Algo.Scalper.Interfaces;
using StockFighter.Api.Models;

namespace StockFighter.Algo.Scalper
{
    public class HedgeState : AlgoStateBase
    {
        public override string StateName { get { return "HedgeState"; } }

        public HedgeState(ILogger logger, StartingParameters startingParameters, Quote bestQuote, Order hedgeOrder)
            : base(logger, startingParameters, bestQuote, hedgeOrder, null)
        {
            this.Logger.Information("Switching to HedgeState");
        }

        public override IScalperOrderAlgo OrderChanged(Order order)
        {
            if (order.Id == this.BuyOrder.Id)
            {
                if (!order.Open)
                {
                    return new QuoteOkState(this.Logger, this.StartingParameters, this.MostRecentQuote);
                }

                this.BuyOrder = order;
            }

            return this;
        }
    }
}
