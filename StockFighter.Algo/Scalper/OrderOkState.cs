﻿using StockFighter.Algo.Scalper.Interfaces;
using StockFighter.Api.Models;
using System.Linq;

namespace StockFighter.Algo.Scalper
{
    public class OrderOkState : AlgoStateBase, IScalperOrderAlgo
    {
        public override string StateName { get { return "OrderOkState"; } }

        public OrderOkState(AlgoStateBase previousState, Order buyOrder, Order sellOrder)
            : base(previousState.Logger, previousState.StartingParameters, previousState.MostRecentQuote, buyOrder, sellOrder)
        {
            this.Logger.Information("Switching to OrderOkState");
        }

        public override IScalperOrderAlgo NewQuote(Quote quote)
        {
            if (quote.QuoteTime <= base.MostRecentQuote.QuoteTime)
            {
                return this;
            }

            if (this.LastMoved(quote) || this.BidSizeTooSmall(quote) || this.BestBidMovedDown(quote))
            {
                this.CancelOrder(this.BuyOrder);

                this.CancelOrder(this.SellOrder);

                return new QuoteOkState(this.Logger, this.StartingParameters, quote);
            }

            if (this.BestBidMovedUp(quote))
            {
                this.CancelOrder(this.BuyOrder);

                var bidPrice = base.GetBidPrice();

                if (bidPrice > 0)
                {
                    var buyOrderResponse = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
                    {
                        Account = this.StartingParameters.AccountName,
                        Direction = "buy",
                        Price = bidPrice,
                        Qty = BidSize,
                        Stock = this.StartingParameters.Symbol,
                        Venue = this.StartingParameters.VenueName,
                        OrderType = "limit"
                    });

                    this.Logger.Information("OrderOkState: Best bid going up, bid order following: Added buy limit order of {0} @ {1}", buyOrderResponse.Qty, buyOrderResponse.Price);

                    if (!buyOrderResponse.RequestStatus.Ok)
                    {
                        this.Logger.Error("Error: {0}", buyOrderResponse.RequestStatus.Error);

                        return this.Abort(quote);
                    }

                    this.BuyOrder = buyOrderResponse;
                }
                else
                {
                    return this.Abort(quote);
                }
            }

            this.MostRecentQuote = quote;

            return this;
        }

        public override IScalperOrderAlgo OrderChanged(Order order)
        {
            if (order.Id == this.SellOrder.Id)
            {
                if (!order.Open)
                {
                    this.Logger.Information("OrderOkState: Sell order hit: filled {0} of {1} @ {2}", order.TotalFilled, order.Qty, order.Price);

                    this.CancelOrder(this.BuyOrder);

                    if (order.Open)
                    {
                        this.CancelOrder(this.SellOrder);
                    }

                    var buyOrderResponse = this.StartingParameters.WebApi.PlaceOrder(new OrderRequest
                    {
                        Account = this.StartingParameters.AccountName,
                        Direction = "buy",
                        Price = this.GetHedgePrice(order.Fills.OrderBy(x => x.Price).First().Price),
                        Qty = order.TotalFilled,
                        Stock = this.StartingParameters.Symbol,
                        Venue = this.StartingParameters.VenueName,
                        OrderType = "limit"
                    });

                    if (!buyOrderResponse.RequestStatus.Ok)
                    {
                        this.Logger.Error("Error: {0}", buyOrderResponse.RequestStatus.Error);

                        return this;
                    }

                    return new HedgeState(this.Logger, this.StartingParameters, this.MostRecentQuote, buyOrderResponse);
                }

                this.SellOrder = order;
            }

            return this;
        }

        private IScalperOrderAlgo Abort(Quote quote)
        {
            base.CancelOrder(this.SellOrder);

            return new QuoteOkState(this.Logger, this.StartingParameters, quote);
        }

        private int GetHedgePrice(int executedPrice)
        {
            return executedPrice - (Spread * 2);
        }
    }
}
