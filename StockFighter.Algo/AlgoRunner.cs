﻿using Serilog;
using StockFighter.Algo.Interfaces;
using StockFighter.Api;
using StockFighter.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace StockFighter.Algo.PlaySellSide
{
    public class AlgoRunner
    {
        private readonly IWebApi api;
        private readonly IAlgorithm algo;
        private readonly ReaderWriterLockSlim algoLockObject = new ReaderWriterLockSlim();
        private readonly ILogger logger;
        private readonly IReadOnlyList<Subscription> subscriptions;

        private bool stop = false;

        public AlgoRunner(ILogger logger, IWebApi webApi, IAlgorithm algo, IEnumerable<Subscription> subscriptions)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (webApi == null)
            {
                throw new ArgumentNullException("webApi");
            }

            if (algo == null)
            {
                throw new ArgumentNullException("algo");
            }

            if (subscriptions == null)
            {
                throw new ArgumentNullException("subscriptions");
            }

            this.logger = logger;
            this.api = webApi;
            this.algo = algo;
            this.subscriptions = subscriptions.ToList();
        }

        public void Stop()
        {
            this.stop = true;
        }

        public void Run()
        {
            this.logger.Information("Start algo");
            this.api.Quote += this.HandleQuote;
            this.api.Execution += this.HandleExecution;

            foreach (var subscription in this.subscriptions)
            {
                this.api.SubscribeToQuotes(subscription.Venue, subscription.Account, subscription.Stock);
                this.api.SubscribeToExecutions(subscription.Venue, subscription.Account, subscription.Stock);
            }

            try
            {
                while (!this.stop)
                {
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "AlgoRunner.Run");

                throw;
            }
            finally
            {
                this.api.Execution -= HandleExecution;
                this.api.Quote -= HandleQuote;

                foreach (var subscription in this.subscriptions)
                {
                    this.api.UnSubscribeFromQuotes(subscription.Venue, subscription.Account, subscription.Stock);
                    this.api.UnSubscribeFromExecutions(subscription.Venue, subscription.Account, subscription.Stock);
                }

                this.logger.Information("Algo finished");
            }
        }

        private void HandleQuote(object sender, QuoteEmbeddedResponse quote)
        {
            if (!quote.RequestStatus.Ok)
            {
                return;
            }

            try
            {
                this.algoLockObject.EnterWriteLock();

                this.algo.NewQuote(quote.Quote);
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "AlgoRunner.HandleQuote");

                throw;
            }
            finally
            {
                this.algoLockObject.ExitWriteLock();
            }
        }

        private void HandleExecution(object sender, Execution execution)
        {
            if (!execution.RequestStatus.Ok)
            {
                return;
            }

            try
            {
                this.algoLockObject.EnterWriteLock();

                this.algo.OrderChanged(execution.Order);
            }
            catch (Exception e)
            {
                this.logger.Fatal(e, "AlgoRunner.HandleExecution");

                throw;
            }
            finally
            {
                this.algoLockObject.ExitWriteLock();
            }
        }
    }
}
